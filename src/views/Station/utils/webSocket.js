import MyWebSocket from '@/utils/webSocket'

let socket = null

function connect(){
  if(socket == null) {
    let url = eval('`' + process.env.VUE_APP_WORKSTATION_WS_HOST + '`') + process.env.VUE_APP_WORKSTATION_WS_URL
    // console.log(url)
    // let url = 'ws://127.0.0.1:8010/api/workstation/'
    socket = new MyWebSocket(url, true, false)
  }
}

function disconnect(){
  socket.close()
  socket = null
}

function on(payload){
  socket.on(payload)
}

function sendText(payload){
  return socket.send_text(payload)
}

function sendFrame(dataFrame, waitResponse=false, timeout=10) {
  return socket.send_text({
    'event': 'DataFrame',
    'data': dataFrame,
    'dataType': 'bytes',
    'waitResponse': waitResponse,
    'timeout': timeout
  })
}

export default {
  connect,
  disconnect,
  on,
  sendText,
  sendFrame,
}
