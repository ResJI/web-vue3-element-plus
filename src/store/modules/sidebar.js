import Cookies from 'js-cookie'

const get_collapse_state = () => {
  const cookie = Cookies.get('isCollapse')
  if (cookie) {
    return cookie === 'true'
  } else {
    return false
  }
}

const state = {
  isCollapse: get_collapse_state()
}

const getters = {}

const mutations = {
  TOGGLE_SIDEBAR: state => {
    state.isCollapse = !state.isCollapse
    if (state.isCollapse) {
      Cookies.set('isCollapse', 'true')
    } else {
      Cookies.set('isCollapse', 'false')
    }
  },
  CLOSE_SIDEBAR: state => {
    state.isCollapse = true
    Cookies.set('isCollapse', 'true')
  }
}

const actions = {
  toggleSidebar({commit}) {
    commit('TOGGLE_SIDEBAR')
  },
  closeSidebar({commit}) {
    commit('CLOSE_SIDEBAR')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
