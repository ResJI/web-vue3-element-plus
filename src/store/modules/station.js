import timeFmt from "@/commonUtils/string/timeFmt";

const state = {

}

const mutations = {

}

const actions = {

}

const cmdLogCard = {
  namespaced: true,
  state: {
    logData: []
  },
  mutations: {
    INSERT_DATA: (state, {displayName, method='手动'}) => {
      let date = new Date()
      let time = timeFmt("YYYY/mm/dd HH:MM:SS", date)
      state.logData.push({
        time,
        method,
        name: displayName,
        result: ''
      })
    },
    UPDATE_RESULT: (state, {index, result}) => {
      let dataList = state.logData
      console.log('dataList', dataList)
      if(dataList.length>0){
        let logData = state.logData[index]
        logData['result'] = result
        dataList.splice(index, 1, logData)
      }
    }
  },
  actions: {
    insertData({ commit, state }, payload) {
      commit('INSERT_DATA', payload)
      return state.logData.length -1
    },
    updateResult({ commit }, payload){
      commit('UPDATE_RESULT', payload)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  modules: {
    cmdLogCard
  }
}
