const state = {
  device: 'desktop'
}

const getters = {}

const mutations = {
  CHANGE_DEVICE: (state, device) => {
    state.device = device
  },
}

const actions = {
  changeDevice({ commit }, device) {
    commit('CHANGE_DEVICE', device)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
