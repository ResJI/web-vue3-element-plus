function hexStr_to_byteArray(str) {
  let byteArray = []
  let cleanData = str.replace(/[^0-9a-f]*/g, '')
  for(let i=0; i<cleanData.length/2; i++){
    let val = parseInt(cleanData.slice(2*i, 2*i+2), 16)
    byteArray.push(val)
  }
  return byteArray
}

function bateArray_to_hexStr(byteArray) {
  let str = ''
  byteArray.forEach((e)=>{
    let hexStr = e.toString(16)
    if(hexStr.lenth < 2){
      hexStr = '0'+hexStr
    }
    str += hexStr
  })
  return str
}

export default {
  hexStr_to_byteArray,
  bateArray_to_hexStr
}
