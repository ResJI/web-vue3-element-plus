import {onMounted, onUnmounted, watch} from 'vue'
import {useStore} from 'vuex'
import {useRoute} from 'vue-router'
export default () => {
  const store = useStore()
  const route = useRoute()
  const { body } = document
  const WIDTH = 992 // refer to Bootstrap's responsive design
  const isMobileCalculate = () => {
    const rect = body.getBoundingClientRect()
    return rect.width - 1 < WIDTH
  }
  const resize = () => {
    if (!document.hidden) {
      const isMobile = isMobileCalculate()
      store.dispatch('app/changeDevice', isMobile ? 'mobile' : 'desktop')
      if (isMobile) {
        store.dispatch('sidebar/closeSidebar')
      }
    }
  }
  onMounted(() => {
    window.addEventListener('resize', resize)
    resize()
  })
  watch(route, () => {
    const isMobile = isMobileCalculate()
    const isCollapse = store.state.sidebar.isCollapse
    if (isMobile && !isCollapse) {
      store.dispatch('sidebar/closeSidebar')
    }
  })
  onUnmounted(() => {
    window.removeEventListener('resize', resize)
  })
}
