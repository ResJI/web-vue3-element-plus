import {createApp} from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/theme-chalk/display.css'
import 'normalize.css/normalize.css' // a modern alternative to CSS resets
import './styles/index.scss'
import './utils/permission' // permission control
import Icon from './icons'

const app = createApp(App)
app.use(router)
app.use(store)
app.use(ElementPlus, {
  // size: 'small',  // 默认控件尺寸
  zIndex: 2000  // 弹出组件的zIndex
})
// app.use(Icon)
app.use(Icon, {registerElementPlusIcons: true})
app.mount('#app')
