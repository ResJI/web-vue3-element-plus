import request from '@/utils/request'

// 登录方法
export function login(username, password) {
  return request({
    url: '/api/login/',
    method: 'post',
    data: {
      username,
      password
    }
  })
}

// 退出方法
export function logout(refreshToken) {
  return request({
    url: '/api/logout/',
    method: 'post',
    data: {
      'refresh_token': refreshToken
    }
  })
}

// 获取当前用户详细信息
export function getSelfInfo() {
  return request({
    url: `api/get-self-info/`,
    method: 'get',
  })
}

// 设置用户详细信息
export function setUserInfo(info) {
  let data = {}
  Object.keys(info).forEach((key) => {
    if(key !== 'id'){
      data[key] = info[key]
    }
  })
  return request({
    url: `api/users/${info.id}/`,
    method: 'put',
    data: data
  })
}

// 设置当前用户头像
export function setUserAvatar(payload) {
  return request({
    url: `api/users/${payload.id}/`,
    method: 'put',
    data: payload.data
  })
}

// 设置当前用户密码
export function setSelfPassword(oldPassword, newPassword) {
  return request({
    url: `api/security/set-self-password/`,
    method: 'post',
    data: {
      old_password: oldPassword,
      new_password: newPassword
    }
  })
}

export default {
  login,
  logout,
  getSelfInfo,
  setUserInfo,
  setSelfPassword,
  setUserAvatar
}
// // 获取用户详细信息
// export function getInfo() {
//   return request({
//     url: '/admin/getInfo/',
//     method: 'get'
//   })
// }

