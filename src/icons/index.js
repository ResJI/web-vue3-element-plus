const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().forEach(requireContext)
requireAll(req)

import SvgIcon from './components/SvgIcon/index'
export default {
  install: (app, options) => {
    app.component('SvgIcon', SvgIcon)
    if (options && options['registerElementPlusIcons']) {
      const iconComponentFiles = require.context('@element-plus/icons/lib', false, /\.js$/)
      const iconComponents = iconComponentFiles.keys().reduce((iconComponents, componentPath) => {
        const iconComponentName = componentPath.replace(/^\.\/(.*)\.\w+$/, '$1')
        const value = iconComponentFiles(componentPath)
        iconComponents[iconComponentName] = value.default
        return iconComponents
      }, {})
      Object.keys(iconComponents).forEach(key => {
        app.component(key, iconComponents[key])
      })
    }
  }
}
