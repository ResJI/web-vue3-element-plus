const {resolve} = require('path')
const Components = require('unplugin-vue-components/webpack')
const { ElementPlusResolver } = require('unplugin-vue-components/resolvers')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = {
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development',
  configureWebpack: {
    devtool: 'source-map',
    resolve: {
      alias: {
        'modules': resolve(__dirname, 'node_modules'),
        '@': resolve(__dirname, 'src'),
        'styles': resolve(__dirname, 'src/style')
      }
    },
    plugins: [
      Components({  // 将Element Plus 设置为按需导入
        resolvers: [ElementPlusResolver()],
      }),
      new BundleAnalyzerPlugin()
    ],
  },
  chainWebpack: config => {
    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()
  }
}
